from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from features.pages.base_page_object import BasePage


class CheckoutPage(BasePage):
    locator_dictionary = {
        "cart_button_css": (By.CSS_SELECTOR, '.icon-btn.ripple.d-sm-b'),
        "header_css": (By.CSS_SELECTOR, '.css-xppemd.e189qfrx0'),
        "product_css": (By.CSS_SELECTOR, '.product-title'),
    }

    def click_cart(self, context):
        element = context.browser.find_element(*self.locator_dictionary['cart_button_css'])
        element.click()

    def is_cart_visible(self, context):
        WebDriverWait(context.browser, 10).until(
            EC.visibility_of_element_located(self.locator_dictionary['header_css']))

    def item(self, context):
        WebDriverWait(context.browser, 10).until(
            EC.visibility_of_element_located(self.locator_dictionary['product_css']))

        element = context.browser.find_element(*self.locator_dictionary['product_css'])
        return element
