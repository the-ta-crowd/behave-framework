from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from features.pages.base_page_object import BasePage


class HomePage(BasePage):
    locator_dictionary = {
        "search_bar_id": (By.ID, 'VisualSearchInput'),
        "accept_cookie_css": (By.CSS_SELECTOR, 'button[data-var="acceptBtn"]'),
    }

    def __init__(self, context):
        BasePage.__init__(
            self,
            context.browser,
            base_url='https://nike.com/nl')

    def accept_cookie(self, context):
        context.browser.implicitly_wait(10)
        WebDriverWait(context.browser, 10).until(
            EC.visibility_of_element_located(self.locator_dictionary['accept_cookie_css'])).click()

    def search(self, article):
        element = self.find_element(*self.locator_dictionary['search_bar_id'])
        element.click()
        element.send_keys(article)
        element.send_keys(Keys.ENTER)
