from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from features.pages.base_page_object import BasePage


class SearchResultPage(BasePage):
    locator_dictionary = {
        "header_css": (By.CSS_SELECTOR, '#Wall .results__body'),
        "add_to_cart_css": (By.CSS_SELECTOR, 'button.add-to-cart-btn'),
        "select_size_xpath": (By.XPATH, '(//*[@class="css-xf3ahq"])[4]'),
        "select_article_xpath": (By.XPATH, '(//*[@class="product-card__img-link-overlay"])[1]')
    }

    def is_page_visible(self, context):
        WebDriverWait(context.browser, 10).until(
            EC.visibility_of_element_located(self.locator_dictionary['header_css']))

    def add_to_cart(self, context):
        context.browser.implicitly_wait(250)

        element = context.browser.find_element(*self.locator_dictionary['select_article_xpath'])
        element.click()

    def select_size_add_to_bag(self, context):
        WebDriverWait(context.browser, 10).until(
            EC.visibility_of_element_located(self.locator_dictionary['add_to_cart_css']))

        size = context.browser.find_element(*self.locator_dictionary['select_size_xpath'])
        size.click()

        add_to_bag = context.browser.find_element(*self.locator_dictionary['add_to_cart_css'])
        add_to_bag.click()
