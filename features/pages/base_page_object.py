from selenium import webdriver


class BasePage(object):

    def __init__(self, browser, base_url='https:www.nike.com/nl'):
        self.base_url = base_url
        self.browser = browser
        self.timeout = 30

    def find_element(self, *loc):
        return self.browser.find_element(*loc)

    def visit(self, url):
        self.browser.get(url)
