from behave import *
from selenium import webdriver

import time



from features.pages.home_page import HomePage
from features.pages.search_result_page import SearchResultPage
from features.pages.checkout_page import CheckoutPage

use_step_matcher("re")

# This is the first sentence that is using regex to match the feature with this step file
# please note the addressing of page objects
@given('ik ben op de nike webstore')
def go_to_site(context):
    context.browser = webdriver.Chrome(executable_path="./chromedriver.exe")
    context.browser.maximize_window()

    page = HomePage(context)
    page.visit("https://nike.com/nl/")
    context.browser.implicitly_wait(250)

    page.accept_cookie(context)
    context.browser.implicitly_wait(250)

# In this step the (.*) is importing a variable from the feature file
# this is very convenient when reusing steps!
@given('ik zoek op "(.*)"')
def search_article(context, article):
    page = HomePage(context)
    page.search(article)

@when('ik een schoen toevoeg aan de winkelwagen')
def add_article(context):
    page = SearchResultPage(context)
    page.is_page_visible(context)
    page.add_to_cart(context)
    page.select_size_add_to_bag(context)
    time.sleep(2)

@then('staat "(.*)" in de winkelwagen')
def go_to_checkout(context, name_shoe):
    page = CheckoutPage(context)
    page.click_cart(context)
    time.sleep(2)
    page.is_cart_visible(context)

    found_text = page.item(context).text
    assert name_shoe in found_text


# remove the Raise NIError when starting to implement these steps your self
@given(u'ik ben in mijn winkelwagen')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given ik ben in mijn winkelwagen')


@when(u'ik "Nike Air Max 90 SE" verwijder')
def step_impl(context):
    raise NotImplementedError(u'STEP: When ik "Nike Air Max 90 SE" verwijder')


@then(u'is mijn winkelwagen leeg')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then is mijn winkelwagen leeg')




