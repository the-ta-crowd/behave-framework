# language: nl
Functionaliteit: : Toevoegen van schoen aan winkelwagen

  # De twee scenarios bestaan uit Gegeven - Als - Dan zinnen
  # Per scenario is er 1 Als stap zodat je weet wat je per scenario aan het testen bent
  # Het eerste scenario is al geimplementeerd in de nike_store_steps.py

  Scenario: Toevoegen van een schoen aan winkelwagen
    Gegeven ik ben op de nike webstore
    En ik zoek op "Nike Air Max 90 SE"
    Als ik een schoen toevoeg aan de winkelwagen
    Dan staat "Nike Air Max 90 SE" in de winkelwagen


  # Aan jou de taak om de onderstaande zinnen te implementeren in het nike_store_steps.py bestand.
  # Uncomment het onderstaande scenario zodat het uitleesbaar wordt voor Behave
  # Let op de uitlijning.
  #
  #  Scenario: Verwijderen van een schoen uit de winkelwagen
  #    Gegeven ik ben in mijn winkelwagen
  #    Als ik "Nike Air Max 90 SE" verwijder
  #    Dan is mijn winkelwagen leeg
