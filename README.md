# ReadMe

## Introduction
Welcome to this project! This project is a small framework using Python together with the Behave library to implement 
TDD. Normally when implementing a TDD methodology, you would not have a finished product already and make your test
(e.g. requirement) in advance and subsequently implement the feature in the product. This should result in a passing test.
For the sake of our precious time we are going to use the Nike-nl homepage instead. 
 
## Prerequisites 
For this project I am using::

- Windows 10
- Python v3.8+
- Behave library
- Selenium library
- Time library
- Chromedriver.exe 87 (Downdload: https://chromedriver.chromium.org/)
- Chrome 87
- PyCharm Community IDE (Download: https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=windows&code=PCC)

It is important that the version of the Chromedriver and the Chromebrowser version match, otherwise this framework
will NOT work. The Chromedriver v87 that I've added for you, is meant for Chrome 87, which is currently the most recent version. 
If you have a newer or older version of Chrome please refer to https://chromedriver.chromium.org/
to download the corresponding Chromedriver for your Chrome version. If you have an older combination of Chrome and Chromedriver
 I cannot guarantee that this will work as sometimes make breaking changes in the webdriver api. If you are using Mac you should
 use the mac64.zip on the Chromedriver website. 
 
## Project Lay-out
The project is composed of 3 main components:
 - Feature file
 - Step file
 - Page objects 
 
The feature files describe the functionality of a feature of a product in a Given-When-Then way (Gherkin language). 
These features can be executed with the help of the Behave library. The sentences in the feature files are (regex-)matched by the 
Behave library in the Step file. 

Each step implementation uses Page objects to describe what it should do (act) or test (assert). 
In the Given steps you state your preconditions of your test. The When step is the actual test (best practice only one when step 
per test). The Then step is the assertion to validate that your test worked. 

In the Page-objects selectors, either by CSS or XPath or Id are used to select an element in the browser that you want to 
manipulate or assert. On a webpage in Chrome you can use Ctrl + Shift + I, or right click on page and use inspect to assert
the elements that are present on the website. Use Ctrl + F to assert that your selector is unique.  

## Exercise 1: Run the first scenario
- Setup your environment with Pycharm, Chrome 87, Python (should also have a Path reference!). 
- Put this repo in a working directory. For example mine is on C:\Users\MyName\Dev\Nike
- In your terminal go to the root folder of this project (nike)
- Run the first scenario by typing 'behave' in your terminal. 

Important: 
As I you can see in the feature file "Als ik een schoen toevoeg aan de winkelwagen" 
I am adding a shoe to the cart. However, when writing this README the only shoe that was available 
was the size EU 37,5. So, if for some reason, Nike ran out of stock (pun intended), you should change the following:

On rule 12 of search_result_page.py, change the 'select_size_38_xpath' selector XPath to an existing XPath. In other words,
change the XPath-selector of the out of stock shoe to the XPath-selector of the in stock shoe. 


## Exercise 2: Implement the second scenario in the nike_store.feature
- Uncomment the second feature scenario and try to implement the sentences in the Step file. 
- All sentences should be matched in the step file, otherwise you will get an error

In order to add a new selector you want to find an UNIQUE (very important) selector. This is very important
otherwise the webdriver doesn't know what to look for (this will give an error and/or warning in your terminal)
On a webpage in Chrome you can use Ctrl + Shift + I, or right click on page and use inspect to assert
the elements that are present on the website. Use Ctrl + F to assert that your selector is unique. 

If you do not know how selectors work, pleaser refer to this guide :): https://gitlab.com/the-ta-crowd/ta_html_css 


## Extracise 3: improve and stabalize the existing tests
For the sake of time I was not able to make this framework perfect. So, if you have time left you can fix some of these
unresolved issues. In some places in the Step file (nike_store_steps.py) I used:  context.browser.implicitly_wait(250). 
This is actually not best practice as your test wait longer than necessary. You instead should wait for an element. However,
in the time that I had to make this framework I did not have the time to find a good element to wait for. So I challenge you 
to fix this :)!  

## Extracise 4: improve the method when selecting a shoe size
Try to make a method that automatically selects an shoe size that is currently in stock. 